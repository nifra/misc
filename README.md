# misc

A few useful things.

## Editor

- My nvim configuration is [here](https://gitlab.com/nifra/dotfiles/-/tree/main/private_dot_config/nvim?ref_type=heads).

## Python

- `Python.code-profile` : import it in VSCode (or Codium) to have a basic and powerful python IDE. You can find all the information on profiles [here](https://code.visualstudio.com/docs/editor/profiles).
- `pyproject.toml` : will be used by VSCode extensions such as Pylint and Ruff as configuration files. Add it to your Python project if you don't already have a `pyproject.toml` file. And please: **read it** to understand what it does, it is configured to also work with [Poetry](https://python-poetry.org/).
